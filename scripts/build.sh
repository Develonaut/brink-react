#!/usr/bin/env bash
# RED='\033[0;31m'
# NC='\033[0m' # No Color
# echo -e "${RED}BEGIN: CONVERTING HTML FILES${NC}"
# find ./build -name \*.html -exec gzip --verbose --best --force {} \;
# echo -e "${RED}SUCCESS! ALL HTML FILES CONVERTED${NC}"
# echo -e "${RED}BEGIN: CONVERTING CSS FILES${NC}"
# find ./build -name \*.css -exec gzip --verbose --best --force {} \;
# echo -e "${RED}SUCCESS! ALL CSS FILES CONVERTED${NC}"
# echo -e "${RED}BEGIN: CONVERTING JS FILES${NC}"
# find ./build -name \*.js -exec gzip --verbose --best --force {} \;
# echo -e "${RED}SUCCESS! ALL JS FILES CONVERTED${NC}"
# https://unix.stackexchange.com/a/309166

echo -e "GZIPPING ALL THE THINGS!";
source ./scripts/gzip.sh;

# extensions=( "" "/static/js" "/static/css" )
# for i in "${extensions[@]}"
# do
#   echo -e "REMOVE .GZ EXTENSION";
#   for file in ./build$i/*.gz; do mv -v "$file" "${file%%.gz}"; done
# done;
git add . && git commit -m 'gzip and deploy using CI deploy script' && git push;



