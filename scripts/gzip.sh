#!/usr/bin/env bash

extensions=( "html" "css" "css.map" "js" "js.map" )
for i in "${extensions[@]}"
do
  find ./build -name \*.$i -exec gzip --verbose --best --keep --force {} \;
done

